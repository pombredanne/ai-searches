package main

import "../ai"

import "fmt"

import "os"
import "strconv"

import "encoding/json"

import (
	"time"
	"math/rand"
)

// from http://golangcookbook.blogspot.co.uk/2012/11/generate-random-number-in-given-range.html
func random(min, max int) int {
    rand.Seed(time.Now().Unix())
    return rand.Intn(max - min) + min
}

func readInt(argNo int, errMsg string) int {
	res, err := strconv.Atoi(os.Args[argNo])
	if err != nil {
		fmt.Println(errMsg)
		os.Exit(1)
	}
	return res
}

func exit(fail string) {
	fmt.Println(fail)
	os.Exit(1)
}

func main () {

	if len(os.Args) < 4 {
		fmt.Println("Need to specify board size and no of blocks and obstacles.")
		fmt.Println("Example: ./generate 4 3 1")
		os.Exit(1)
	}

	size := readInt(1, "Invalid size")
	blocks := readInt(2, "Invalid no. of blocks")
	obstacles := readInt(3, "Invalid no. of obstacles")

	// fmt.Printf("Size: %d, Blocks: %d, Obstacles: %d\n", size, blocks, obstacles)

	if blocks > size {
		exit("Invalid configuration. Blocks must be <= size")
	}

	world := make([][]bool, size)
	for i := range world {
		world[i] = make([]bool, size)
	}

	var board ai.Board

	blks := make([]ai.Position, 0, blocks)
	obstcs := make([]ai.Position, 0, obstacles)
	agent := ai.Position{random(1, size), random(1,size)}

	world[agent.X-1][agent.Y-1] = true

	i:=0
	for i<blocks {
		x,y := random(0,size), random(0, size)
		if world[x][y] == false {
			world[x][y] = true
			blks = append(blks, ai.Position{x+1,y+1})
			i++
		}
	}

	i=0
	for i<obstacles {
		x,y := random(0,size), random(0, size)
		if world[x][y] == false {
			world[x][y] = true
			obstcs = append(obstcs, ai.Position{x+1,y+1})
			i++
		}
	}

	board = ai.Board{Size: size, Blocks: blks, Agent: agent, Obstacles: obstcs}

	output, jsonerr := json.Marshal(board)

	if jsonerr != nil {
		fmt.Println(jsonerr)
		exit("JSON encoding of board error")
	}

	node, _ := ai.Astar(&board)

	// print to output
	fmt.Printf("%d|%s\n", node.GetCost(), output)
}
