package ai

import "container/heap"

type ANode struct {
	node *Node
	priority int
}

// priority queue from Go example: http://golang.org/pkg/container/heap/
type PriorityQueue []*ANode

func (pq PriorityQueue) Len() int {
	return len(pq)
}

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueue) Push(x interface{}) {
	item := x.(*ANode)
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func manhattanDistance(b *Board) int {
	dist := 0
	dist += abs(b.Agent.X - b.Size) + abs(b.Agent.Y - b.Size)
	destX := 2
	destY := b.Size - len(b.Blocks) + 1

	for _, block := range b.Blocks {
		dist += abs(block.X - destX) + abs(block.Y - destY)
		destY--
	}
	return dist
}

func Astar(b *Board) (*Node, int) {

	todo := &PriorityQueue{}
	heap.Init(todo)

	initialNode := &ANode{&Node{b, nil, -1, 0}, 0}

	heap.Push(todo, initialNode)

	visited:=make(map[string]bool)
	visited[b.toUglyString()] = true

	expandDirection := func(n *Node, dir int) *ANode {
		brd := n.b.clone()
		if brd.move(dir) && !visited[brd.toUglyString()] {
			newNode := &Node{brd, n, dir, n.count+1}
			h := newNode.count + manhattanDistance(newNode.b)
			return &ANode{newNode, h}
		}
		return nil
	}

	expanded := 0
	for len(*todo) > 0 {
		anode := heap.Pop(todo).(*ANode)
		node := anode.node

		expanded++

		if node.b.isGoal() {
			return node, expanded
		}

		var next *ANode
		var directions = [...]int{UP, LEFT, DOWN, RIGHT}

		for _, dir := range directions {
			next = expandDirection(node, dir)
			if next != nil {
				heap.Push(todo, next)
				visited[next.node.b.toUglyString()] = true
			}			
		}
	}

	return nil, expanded
}