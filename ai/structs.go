package ai

import "fmt"

const (
	LEFT = iota
	RIGHT = iota
	UP = iota
	DOWN = iota
)

type Position struct {
	X, Y int
}

type Board struct {
	Size int
	Blocks []Position
	Agent Position
	Obstacles []Position
}

func letterFromIndex(index int) byte {
	return byte(index+65)
}

func stringDirection(direction int) string {
	switch direction {
	case LEFT:
		return "LEFT"
	case RIGHT:
		return "RIGHT"
	case UP:
		return "UP"
	case DOWN:
		return "DOWN"
	}
	return "unknown direction"
}

type Node struct {
	b *Board
	parent *Node
	move int
	count int //cost
}

func (n *Node) GetCost() int {
	return n.count
}

func (n *Node) PrintSolution() {
	if n == nil {
		fmt.Println("nil node...")
		return
	}

	fmt.Printf("Move count: %d\n", n.count)
	
	moves := make([]int, n.count)
	ind:=n.count-1
	for node:=n; node.move > -1; node=node.parent {
		moves[ind]=node.move
		ind--
	}

	for _, move := range moves {
		fmt.Printf("Move %s\n", stringDirection(move))
	}

	fmt.Print(n.b.ToString())
}