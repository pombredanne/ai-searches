package ai

import "container/list"

func Bfs(b *Board) (*Node, int) {

	todo := list.New()
	initialNode := &Node{b, nil, -1, 0}

	todo.PushBack(initialNode)

	visited:=make(map[string]bool)
	visited[b.toUglyString()] = true

	expandDirection := func(n *Node, dir int) *Node {
		brd := n.b.clone()
		if brd.move(dir) && !visited[brd.toUglyString()] {
			return &Node{brd, n, dir, n.count+1}
		}
		return nil
	}

	expanded := 0
	for element := todo.Front(); element != nil; element = todo.Front() {
		node := element.Value.(*Node)
		todo.Remove(element)

		expanded++

		if node.b.isGoal() {
			return node, expanded
		}

		var next *Node
		directions := [...]int{UP, LEFT, DOWN, RIGHT}

		for _, dir := range directions {
			next = expandDirection(node, dir)
			if next != nil {
				todo.PushBack(next)
				visited[next.b.toUglyString()] = true
			}
		}
	}

	return nil, expanded
}